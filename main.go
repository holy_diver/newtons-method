package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Hi, this is a test for Newton's Method of finding an approximation of any positive number inside a SquareRoot")
	fmt.Print("math.Sqrt(999) = ")
	fmt.Println(sqrt(999))
	fmt.Print("newton(2, 2, 0.00000005) = ")
	fmt.Println(newton(999, 2, 0.00000005))
}

func newton(number float64, x0 float64, epsilon float64) float64 {
	//f(x) = x^2 - number
	//f'(x) = 2x
	//x1 = x0 - (f(x) / f'(x))
	var x1 = x0 - 2*epsilon
	var prev = x0
	for !(x1 >= prev-epsilon) || !(x1 <= prev+epsilon) {
		prev = x0
		x1 = x0 - f(x0, number)/f_d(x0)
		x0 = x1
	}
	return x1
}

func f(x float64, c float64) float64 {
	return x*x - c
}

func f_d(x float64) float64 {
	return 2 * x
}

func sqrt(number float64) (ret float64) {
	ret = math.Sqrt(number)
	return
}
